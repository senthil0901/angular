import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeepipePipe } from './employeepipe.pipe';
import { EmployeecountComponent } from './employeecount/employeecount.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';


@NgModule({
  declarations: [
    AppComponent,
    EmployeelistComponent,
    EmployeepipePipe,
    EmployeecountComponent,
    LifecycleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
