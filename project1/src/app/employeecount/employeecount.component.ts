import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employeecount',
  templateUrl: './employeecount.component.html',
  styleUrls: ['./employeecount.component.css']
})
export class EmployeecountComponent implements OnInit {
  @Input()  
  all: number;
    @Input()
    male: number;
    @Input()
    female: number;

  constructor() { 
    console.log("In constructor");
  }

  ngOnChanges(){
    console.log("in ngOnChanges");
  }
  ngOnInit(): void {
    console.log("in ngOnInit");
  }

  ngDoCheck(){
    console.log("in ngDoCheck");
  }

  ngAfterContentInit(){
      console.log("in ngAfterContentInit");
  }

  ngAfterContentChecked(){
    console.log("in ngAfterContentChecked");
}
ngAfterViewInit(){
  console.log("in ngAfterViewInit");
}
ngAfterViewChecked(){
  console.log("in ngAfterViewChecked");
}
ngOnDestroy(){
  console.log("in destroy");
}
  selectedRadioButtonValue: string = 'All';

  @Output()
  countRadioButtonSelectionChanged: EventEmitter<string> = new EventEmitter<string>();

  onRadioButtonSelectionChange(){
    this.countRadioButtonSelectionChanged.emit(this.selectedRadioButtonValue);
    console.log(this.selectedRadioButtonValue);
  }
}
