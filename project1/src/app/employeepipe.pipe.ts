import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'employeepipe'
})
export class EmployeepipePipe implements PipeTransform {

  transform(value:string, gender: string): string {
    if(gender.toLowerCase()=='Male')
      return "MR."+value;
      else
      return "MS."+value;
  }

}
