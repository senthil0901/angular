import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Templatedriven1Component } from './templatedriven1.component';

describe('Templatedriven1Component', () => {
  let component: Templatedriven1Component;
  let fixture: ComponentFixture<Templatedriven1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Templatedriven1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Templatedriven1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
