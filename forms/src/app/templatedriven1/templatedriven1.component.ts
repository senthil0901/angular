import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { Router } from '@angular/router';

@Component({
  selector: 'app-templatedriven1',
  templateUrl: './templatedriven1.component.html',
  styleUrls: ['./templatedriven1.component.css']
})
export class Templatedriven1Component implements OnInit {

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  personModel=new Person("Ramu","ramu@gmail.com",74788383);

  submitted=false;

  onSubmit(){
    console.log("Submitted");
    this.submitted=true;
    if(this.submitted==true){
        this.route.navigate(["/success"]);
    }
  }
}
