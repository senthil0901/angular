import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ForbiddenNameValidator } from '../shared/user-name.validator';
import { PasswordValidator } from '../shared/password.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {
 

  //2.Creating Form Model
 /* registrationForm=new FormGroup({
    username: new FormControl('Sam'),
    password:new FormControl(''),
    confirmPassword:new FormControl('')
  });
*/

  //3.Nesting Form groups
  /*registrationForm=new FormGroup({
    username: new FormControl('Sam'),
    password:new FormControl(''),
    confirmPassword:new FormControl(''),
    address:new FormGroup({
      city:new FormControl(''),
      state:new FormControl(''),
      postalCode:new FormControl('')
    })
  });*/

  //4. Managing control values - to all values
 /* loadApiData(){
    this.registrationForm.setValue({
      username:'Ram',
      password:'test',
      confirmPassword:'test',
      address:{
        city:'Chennai',
        state:'Tamilnadu',
        postalCode:672552
      }
    });
  }*/

  //only particular values
  loadApiData(){
    this.registrationForm.patchValue({
      username:'Ram',
      password:'test',
      confirmPassword:'test'
    });
 }

 //5. FormBuilder
 /*constructor(private fb: FormBuilder){}

 registrationForm=this.fb.group({
  username:['Raj'],
   password:[''],
   confirmPassword:[''],
   address:this.fb.group({
    city:['chennai'],
    state:['tamilnadu'],
    postalCode:['63637']
  })
 }); */

 //6. Simple Validation
/* constructor(private fb: FormBuilder){}

 registrationForm=this.fb.group({
   username:['Raj',[Validators.required,Validators.minLength(3)]],
   password:[''],
   confirmPassword:[''],
   address:this.fb.group({
    city:[''],
    state:[''],
    postalCode:['']
  })
 });

 get username(){
   return this.registrationForm.get('username');
 }
 */
 


 //7. Custom Validation
 /*constructor(private fb: FormBuilder){}

 registrationForm=this.fb.group({
   username:['Raj',[Validators.required,Validators.minLength(3),ForbiddenNameValidator]],
   password:[''],
   confirmPassword:[''],
   address:this.fb.group({
    city:[''],
    state:[''],
    postalCode:['']
  })
 });

 get username(){
   return this.registrationForm.get('username');
 }
 */

 //8. Cross Field validation
/* constructor(private fb: FormBuilder){}

 registrationForm=this.fb.group({
   username:['Raj',[Validators.required,Validators.minLength(3),ForbiddenNameValidator]],
   password:[''],
   confirmPassword:[''],
   address:this.fb.group({
    city:[''],
    state:[''],
    postalCode:['']
  })
 }, {validator: PasswordValidator});

 get username(){
   return this.registrationForm.get('username');
 }*/

 //9. Conditional validation
 /*constructor(private fb: FormBuilder){}

 registrationForm: FormGroup;



 ngOnInit(): void {
  this.registrationForm=this.fb.group({
    username:['Raj',[Validators.required,Validators.minLength(3),ForbiddenNameValidator]],
    email:[''],
    subscribe:[false],
    password:[''],
    confirmPassword:[''],
    address:this.fb.group({
     city:[''],
     state:[''],
     postalCode:['']
   })
  }, {validator: PasswordValidator});

  this.registrationForm.get('subscribe').valueChanges
      .subscribe(checkedValue => {
        const email = this.registrationForm.get('email');
        if (checkedValue) {
          email.setValidators(Validators.required);
        } else {
          email.clearValidators();
        }
        email.updateValueAndValidity();
      });
 }

 get username(){
  return this.registrationForm.get('username');
}

get email() {
 return this.registrationForm.get('email');
}
*/ 

 //9. Dynamic Form controls
 
 constructor(private fb: FormBuilder,private route:Router){}

 registrationForm: FormGroup;

 get username(){
  return this.registrationForm.get('username');
}

get email() {
 return this.registrationForm.get('email');
}

get alternateEmails() {
  return this.registrationForm.get('alternateEmails') as FormArray;
}

addAlternateEmail() {
  this.alternateEmails.push(this.fb.control(''));
}

 ngOnInit(): void {
  this.registrationForm=this.fb.group({
    username:['Raj',[Validators.required,Validators.minLength(3),ForbiddenNameValidator]],
    email:[''],
    subscribe:[false],
    password:[''],
    confirmPassword:[''],
    address:this.fb.group({
     city:[''],
     state:[''],
     postalCode:['']
   }),
   alternateEmails:this.fb.array([])
  }, {validator: PasswordValidator});

  this.registrationForm.get('subscribe').valueChanges 
      .subscribe(checkedValue => {
        const email = this.registrationForm.get('email');
        if (checkedValue) {
          email.setValidators(Validators.required);
        } else {
          email.clearValidators();
        }
        email.updateValueAndValidity();
      });
 }

//10. Form submission

submitted=false;
onSubmit() {
  console.log("clicked");
  this.submitted = true;
  if(this.submitted==true){
  this.route.navigate(['/regSuccess']);
  }
}

}


