import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegSuccessComponent } from './reg-success/reg-success.component';
import { TemplatedrivenComponent } from './templatedriven/templatedriven.component';
import { ReactiveComponent } from './reactive/reactive.component';
import { Templatedriven1Component } from './templatedriven1/templatedriven1.component';
import { ReactiveformComponent } from './reactiveform/reactiveform.component';


const routes: Routes = [
  {
    path:'template1',
    component: Templatedriven1Component
  },
  {
    path:'success',
    component: RegSuccessComponent
  },
  {
    path:'reactiveform',
    component: ReactiveformComponent
  },
  {
    path:'regSuccess',
    component: RegSuccessComponent
  },
  {
    path:'template',
    component: TemplatedrivenComponent
  },
  {
    path:'reactive',
    component: ReactiveComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
   