import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { Router, Route } from '@angular/router';

@Component({
  selector: 'app-templatedriven',
  templateUrl: './templatedriven.component.html',
  styleUrls: ['./templatedriven.component.css']
})
export class TemplatedrivenComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit(): void {
  }

  topics=['Angular','React','Vue'];

  userModel=new User("Bob","bob@gmailcom",384774646,"default","morning",true);

  topicHasError = true;

  validateTopic(value) {
    if (value === 'default') {
      this.topicHasError = true;
    } else {
      this.topicHasError = false;
    }
  }

  submitted = false;

  onSubmit() {
    console.log("clicked");
    this.submitted = true;
    if(this.submitted==true){
    this.route.navigate(['/regSuccess']);
    }
  }
}     
