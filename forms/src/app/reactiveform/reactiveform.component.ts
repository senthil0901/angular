import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Password1Validator } from '../password1.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reactiveform',
  templateUrl: './reactiveform.component.html',
  styleUrls: ['./reactiveform.component.css']
})
export class ReactiveformComponent implements OnInit {

  constructor(private fb: FormBuilder,private route:Router) { }

  ngOnInit(): void {
  }

  registrationForm=this.fb.group({
      username:['Raja',[Validators.required,Validators.minLength(4)]],
      password:[''],
      confirmPassword:[''],
      email:['',[Validators.required,Validators.email]],
      alternateEmails: this.fb.array([])
  }, {validator: Password1Validator});

  get username(){
    return this.registrationForm.get('username');
  }

  get email(){
    return this.registrationForm.get('email');
  }

  get alternateEmails(){
    return this.registrationForm.get('alternateEmails') as FormArray;
  }

  addAlternateEmail(){
    this.alternateEmails.push(this.fb.control(''));
  }

 /* registrationForm=new FormGroup({
    username:new FormControl("sam"),
    password:new FormControl(''),
    confirmPassword:new FormControl('')
  });*/

 /* loadApiData(){
    this.registrationForm.setValue({
      username:"Ram",
      password:"abcd",
      confirmPassword:"abcd"
    })
  }*/

  loadApiData(){
    this.registrationForm.patchValue({
      username:"Ram",
      password:"abcd"
    })
  }


  submitted=false;

  onSubmit(){
    console.log("Submitted");
    this.submitted=true;
    if(this.submitted==true){
        this.route.navigate(["/success"]);
    }
  }
}
