Jenkins

Process before CI
   We have group of developers who are taking changes that are present in Source code repository and when entire source code of the application is written it will be built by tools like ant,maven etc.
    After that the build appl will be deployed on the test server for testing, if there is any bug in code the developer will notify with the feedback loop and if there is no bug then appl is deployed in production for release.
    
Problems before CI
  1. Developers have to wait till the complete software is developed for test results
  2. Since the entire source code of the appl is first build and then it is tested. So if any bug in the code,developer has to go through the entire source code of the application.
   3. Developers were actually wasting lot of time in locating and fixing bugs instead of building new appl.So software delivery process was slow
   4. Continous feedback related to build failure, test status was not present, due to which the developers is unaware of how the appl is doing 

Continous Integration
     We have multiple developers if any one of them makes any commit to the source code present in repository. The code will be pulled, it will be build, tested and deployed.
    So advantage is any commit made to code is build and tested due to which any bug in the code, the developer actually know where the bug is present and which commit cause the error, so no need to go through the entire source code of application, just need to check that commit that introduce the bug.

What is CI?
    The developers are required to make frequent commit to the source code,because of that any changes made in the source code will pull by CI server and that code will be build, then it will deploy on test server for testing and once testing is done it also deployed in production for release and developers getting continuous feedback on the run.

Continuous Integration tool
1. Jenkins
2. Buildbot
3. Travis CI
4. Bamboo

1. Create a project 
2. Right click project -- Team -- Share project
3. Create a local repoistory(it will push in remote repoistory at the time of commit)
   Click create -- c:/senthil.kumart/git -- create a folder under git
    click finish
4. Right click project -- Team 
    Select all files from unstaged and put into staged files
    Give commit message
   Click commit & push
5. Give URI: from GIT url
   port: 
   Username: hcl username 
   password: hcl pwd
   click next
     click finish
Now the project will be pushed inside gitlab

6.Build the project in jenkins
   1. Download jenkins.war from google https://updates.jenkins-ci.org/download/war/ 2.2.04
   2. Go to cmd prompt where jenkins.war is present and run the jenkins in specific port
        >java -jar jenkins.war --httpPort=2000
             Copy the generated password
   3. Run jenkins in browser as http://localhost:2000
   4. Login with generated password -- click login
   5. Click Installed plugin -- all plugins will be installed
   6. Give username and password as all admin
   7. Click save and continue
   8. Click Save and finish
   9. Click Start Jenkins
   10. Click new item
         Enter item name: Spring project
            Click Freestyle project
            Click Ok
   11.Goto SCM
         Git: Copy and paste the GIT url
              Click Add --- Jenkins -- Give hcl username and password --  Click Save
               Select ur name from drop menu
               Click Save
   12. Goto Jenkins -- Manage Jenkins -- Global tool Configuration
         * Input JDK first -- Uncheck Install automatically
             JDK NAME: Java
              Path: JDK Path
         * Click Maven 
             Maven name: Maven
             Path: Maven Path
         * Click Save
   13. Go to project dashboard -- Select the dropmenu from project and click build now
     ----creates the workpace and put project inside jenkins
   14. Once the build was successful Go to Confgiure -- Source code mgt -- Build option -- choose Invoke top level maven targets -- Maven version (select the name given for maven installation)
     Goals: clean install
     click Advanced
     POM: give the projectname/pom.xml (which is present inside users/.jenkins/worspace/project)
     Click save -- CLICK apply
  15. Again go to project and once again Build now
        First build is to create workspace and second build to run the project with pom.xml
     Now the build is success and we can run in eclipse

                
To take junit test report from jenkins (in case we written any test cases)
  1. In case if we wriiten test case then jenkins create surefire reports in users/jenkins/workspace/jenkinsproject/project/target/surefire-reports
  2. Go to jenkins project -- configure -- SCM -- Post build action --Publish junit test result report -- 
  Test report XML's: projectname\target\surefire-reports\*.xml
  Click Save
3. Go to project dahboard --  click success build file --- Click Test Result 
         which shows how many test cases passes and failed

To run sonarqube in jenkins for code coverage
   1. Go to jenkins project -- confgiure -- SCM -- Build -- choose Invoke top level maven targets
      Creates another maven version
      Maven version: maven (select the name given at maven installation)
      Goals: sonar:sonar
      POM: give the project/pom.xml (which is present inside users/.jenkins/worspace/project)
      Properties: sonar:sonar -Dsonar.host.url=http://localhost:9000
      Click Save
   2. Build now the project
   3. It will generate a link where it goes to the Sonarqube dashboard which







Errors

1. There were errors checking the update sites: SSLHandshakeException: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target

goto - Manager Jenkins - Manage plugins - Advanced tab - In Update site change https to http - Submit