package com.pack;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main1 {

	public static void main(String[] args) {
		// ApplicationContext context = new FileSystemXmlApplicationContext("hello.xml");
		 ApplicationContext context = new ClassPathXmlApplicationContext("hello.xml");
		 Example e = (Example) context.getBean("hello");
		 System.out.println(e.getMessage());
	}

}
