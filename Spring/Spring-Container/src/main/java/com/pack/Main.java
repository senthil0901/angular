package com.pack;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

public class Main {

	public static void main(String[] args) {
		//Resource res=new ClassPathResource("hello.xml");
		Resource res=new FileSystemResource("hello.xml");
		BeanFactory factory=new XmlBeanFactory(res);
		Example e = (Example) factory.getBean("hello");
		System.out.println(e.getMessage());
	}

}
