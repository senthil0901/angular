package com.pack.anno;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class UserConfig {
	   @Bean
	   @Primary
	   public User getAdminUser() {
	      return new Admin();
	   }

	   @Bean
	   public User getCreatorUser() {
	      return new Creator();
	   }
}
