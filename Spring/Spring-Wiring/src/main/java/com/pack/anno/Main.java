package com.pack.anno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans3.xml");
		Person p1=(Person)context.getBean("person");
		System.out.println(p1.getPname()+" "+p1.getManager().getName()+" "+p1.getManager().getQualification());
	}

}
