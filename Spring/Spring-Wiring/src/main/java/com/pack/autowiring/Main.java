package com.pack.autowiring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Main {

	public static void main(String[] args) {
		 ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		 Student stud = (Student) context.getBean("student");
		 System.out.println(stud.getName());
		 System.out.println(stud.getAddress().getCountry());
	}

}
