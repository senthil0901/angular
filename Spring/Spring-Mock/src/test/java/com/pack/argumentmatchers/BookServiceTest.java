package com.pack.argumentmatchers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.pack.argumentmatcher.Book;
import com.pack.argumentmatcher.BookRepository;
import com.pack.argumentmatcher.BookService;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {
	
	@InjectMocks
	private BookService bookService;
	
	
	@Mock
	private BookRepository bookRepository;
	
	//Step 1
	@Test
	public void testUpdatePrice() {
		Book book1 = new Book("1234", "Mockito In Action", 600, LocalDate.now());
		Book book2 = new Book("1234", "Mockito In Action", 500, LocalDate.now());
		//when(bookRepository.findBookById(any())).thenReturn(book1);
		when(bookRepository.findBookById(any(String.class))).thenReturn(book1); //here update book1 from 600 to 500
		bookService.updatePrice("1234", 500);
		verify(bookRepository).save(book2);
	}
	
	// Argument Matchers should be provided for all arguments  
	// Argument Matchers cant be used outside stubbing/verification
	//Step 2
	@Test
	public void testInvalidUseOfArgumentMatchers() {
		Book book = new Book("1234", "Mockito In Action", 600, LocalDate.now());
		//when(bookRepository.findBookByTitleAndPublishedDate("Mockito In Action", any())).thenReturn(book); //error
		when(bookRepository.findBookByTitleAndPublishedDate(eq("Mockito In Action"), any())).thenReturn(book);
		Book actualBook = bookService.getBookByTitleAndPublishedDate("Mockito In Action", LocalDate.now());
		//Book actualBook = bookService.getBookByTitleAndPublishedDate(eq("Mockito In Action"), any()); //error
		assertEquals("Mockito In Action", actualBook.getTitle());
	}
	
	//Step 3
	@Test
	public void testSpecificTypeOfArgumentMatchers() {
		Book book = new Book("1234", "Mockito In Action", 600, LocalDate.now());
		when(bookRepository.findBookByTitleAndPriceAndIsDigital(anyString(), anyInt(), anyBoolean())).thenReturn(book);
		Book actualBook = bookService.getBookByTitleAndPriceAndIsDigital("Mockito In Action", 600, true);
		assertEquals("Mockito In Action", actualBook.getTitle());
	}
	
	//Step 4
	@Test
	public void testCollectionTypeArgumentMatchers() {
		List<Book> books = new ArrayList<>();
		Book book = new Book("1234", "Mockito In Action", 600, LocalDate.now());
		books.add(book);
		bookService.addBooks(books);
		verify(bookRepository).saveAll(anyList()); // anySet, anyMap, anyCollection
	}
	
	//Step 5
	@Test
	public void testStringTypeArgumentMatchers() {
		Book book = new Book("1234", "Mockito In Action", 600, LocalDate.now());
		when(bookRepository.findBookByTitleAndPriceAndIsDigital(contains("Action"), anyInt(), anyBoolean())).thenReturn(book);
		Book actualBook = bookService.getBookByTitleAndPriceAndIsDigital("JUnit 5 In Action", 600, true);
		assertEquals("Mockito In Action", actualBook.getTitle());
	}
	
}