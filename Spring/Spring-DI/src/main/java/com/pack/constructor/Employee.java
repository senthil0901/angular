package com.pack.constructor;

public class Employee {
   private int id;
   private String name;
   private Double salary;

public Employee(int id, String name, Double salary) {
	super();
	this.id = id;
	this.name = name;
	this.salary = salary;
}

public Employee() {
	super();
	// TODO Auto-generated constructor stub
}

@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
}
   
}
