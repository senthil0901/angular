package com.pack.setter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {

	public static void main(String[] args) {
		 ApplicationContext context = new ClassPathXmlApplicationContext("hello.xml");
		 Employee emp = (Employee) context.getBean("emp1");
		 System.out.println(emp);
	}

}
