package com.employeemanagement.test;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.employeemanagement")
public class TestBeanConfig {

}
