//https://www.infoworld.com/article/3543268/junit-5-tutorial-part-2-unit-testing-spring-mvc-with-junit-5.html?page=3
package com.employeemanagement.test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.employeemanagement.controller.EmployeeController;
import com.employeemanagement.dao.EmployeeDao;
import com.employeemanagement.model.Employee;
import com.employeemanagement.service.EmployeeService;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
// @RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = { TestBeanConfig.class })
public class EmployeeTest {
	@Autowired
	EmployeeService service;

	@Mock
	HttpServletRequest request;
	@Mock
	EmployeeDao dao;
	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@InjectMocks
	private EmployeeController employeeController;

	Employee emp;

	@Before
	public void setUp() {

		emp = new Employee();
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
	@Test
	public void testInsertAndUpdateEmployee() throws Exception {
		Employee empl = new Employee(0, "drishanaa", "F", LocalDate.now(), new BigDecimal("0.03"), "aaa");

		mockMvc.perform(post("/save").param("id", String.valueOf(empl.getId())).param("name", empl.getName())
				.param("gender", empl.getGender()).param("salary", String.valueOf(empl.getSalary()))
				.param("joiningDate", String.valueOf(empl.getJoiningDate())).param("email", empl.getEmail())
				.flashAttr("employee", empl)).andExpect(status().isFound())
				.andExpect(view().name("redirect:/list"));
		
	}

	@Test
	public void testUpdateEmployee() throws Exception {
		Employee empl = new Employee(3, "drishanaa", "F", LocalDate.now(), new BigDecimal("0.03"),
				"revathi@gmail.com");

		mockMvc.perform(post("/save").param("id", String.valueOf(empl.getId())).param("name", empl.getName())
				.param("gender", empl.getGender()).param("salary", String.valueOf(empl.getSalary()))
				.param("joiningDate", String.valueOf(empl.getJoiningDate())).param("email", empl.getEmail())
				.flashAttr("employee", empl)).andExpect(status().isFound()).andExpect(view().name("redirect:/list"));

	}

	@Test
	public void listEmployees() {

		 assertEquals(3, service.findAllEmployees().size());
}

	@Test
	public void registrationTest() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("registration"));
	}

	@Test
	public void listEmployeeTest() throws Exception {
		mockMvc.perform(get("/list")).andExpect(status().isOk()).andExpect(view().name("allemployees"));
	}

	@Test
	public void testdeleteEmployee() throws Exception {

		this.mockMvc.perform(get("/deleteEmployee").param("id","6"))
				.andExpect(status().isMovedTemporarily()).andExpect(view().name("redirect:/list"));

	}

	@Test
	public void testEditEmployee() throws Exception {
		this.mockMvc.perform(get("/editEmployee").param("id", "5")).andExpect(status().isOk())
				.andExpect(view().name("registration"));

	}
}
