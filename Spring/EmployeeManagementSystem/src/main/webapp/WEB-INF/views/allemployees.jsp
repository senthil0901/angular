<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Management System</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.css" />



<style>
tr:first-child {
	font-weight: bold;
	background-color: #C6C9C4;
}

body {
	margin-top: 1%;
	background: #e5e6e6;
}
.bt{
margin-top:3%;
}
</style>

</head>


<body class="col-sm-offset-1 col-md-10">
	<div class="row">
		<div class="col-md-6">
			<h2><strong>List of Employees</strong></h2>
		</div>
		<div class="col-md-6">
			<a class="pull-right btn btn-success bt" href="<c:url value='/' />">Add
				New Employee</a>

		</div>
	</div>
	<table class="table datatable table-bordered">
		<tr>
			<td>Name</td>
			<td>Joining Date</td>
			<td>Salary</td>
			<td>Gender</td>
			<td>Email</td>
			<td></td>
		</tr>
		<c:forEach items="${employees}" var="employee">

			<tr>
				<td>${employee.name}</td>
				<td>${employee.joiningDate}</td>
				<td>${employee.salary}</td>
				<td>${employee.gender}</td>
				<td>${employee.email}</td>
				<td>
				<a class="btn btn-info" href="editEmployee?id=${employee.id}">Edit</a> 
				<a class="btn btn-danger" href="deleteEmployee?id=${employee.id}">Delete</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	<br />

	<script src="https://code.jquery.com/jquery-1.12.4.js"
		integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
		integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-darkness/jquery-ui.css">
</body>
</html>