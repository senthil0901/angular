<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Registration Form</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<style>
.error {
	color: #ff0000;
}

body {
	background: #e5e6e6;
}
</style>

</head>

<body>
	<div class="col-sm-offset-1">
		<h2>
			<strong>Registration Form</strong>
		</h2>
	</div>
	<br />
	<br />
	<div class="container">
		<div class="col-sm-offset-2 col-md-8">
			<form:form action="save" method="POST" modelAttribute="employee">
				<form:input type="hidden" path="id" id="id" />
				<div class="row">
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name:</label>
						<div class="col-sm-10">
							<form:input path="name" id="name" class="form-control" />
							<form:errors path="name" cssClass="error" />
						</div>
					</div>
				</div>
				<br />
				<br />


				<div class="row">
					<div class="form-group">
						<label class="control-label col-sm-2" for="joiningDate">Joining
							Date:</label>
						<div class="col-sm-10">
							<form:input type="date" path="joiningDate" id="joiningDate"
								class="form-control" />
							<form:errors path="joiningDate" cssClass="error" />
						</div>
					</div>
				</div>
				<br />
				<br />
				<div class="row">
					<div class="form-group">
						<label class="control-label col-sm-2" for="salary">Salary:</label>
						<div class="col-sm-10">
							<form:input path="salary" id="salary" class="form-control" />
							<form:errors path="salary" cssClass="error" />
						</div>
					</div>
				</div>
				<br />
				<br />
				<div class="row">
					<div class="form-check" style="margin-right: 10px">
						<div class="form-group">
							<form:label path="" class="control-label col-sm-2"
								style="margin-right:10px" for="gender" value="Gender:">Gender:</form:label>


							Male
							<form:radiobutton path="Gender" value="Male"
								class="form-check-input" />

							Female
							<form:radiobutton path="Gender" value="Female"
								class="form-check-input" />
									<form:errors path="gender" cssClass="error" />
						</div>
					</div>
				</div>
				<br />

				<div class="row">
					<div class="form-group">
						<label class="control-label col-sm-2" for="ssn">Email:</label>
						<div class="col-sm-10">
							<form:input path="email" id="email" class="form-control" />
							<form:errors path="email" cssClass="error" />
						</div>
					</div>
				</div>
				<br />
				<br />


				<c:set var="id" scope="session" value="${employee.id}" />
				<c:if test="${id!=0}">
					<button class="btn btn-warning" type="submit">Update</button>
				</c:if>
				<c:if test="${id==0}">
					<button class="btn btn-success" type="submit">Register</button>
				</c:if>
				<button class="btn btn-danger" onclick="location.href='list'"
					type="button">Cancel</button>
			</form:form>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.12.4.js"
		integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
		integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-darkness/jquery-ui.css">
</body>
</html>