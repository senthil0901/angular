package com.employeemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.employeemanagement.dao.EmployeeDao;
import com.employeemanagement.model.Employee;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao dao;
	
	

	public void saveEmployee(Employee employee) {
		
		dao.saveEmployee(employee);
	}
	
	public void updateEmployee(Employee employee) {
		dao.updateEmployee(employee);
		}
	

	
	
	public List<Employee> findAllEmployees() {
		return dao.findAllEmployees();
	}

	

	 
    public Employee getEmployee(int empid) {
        return dao.getEmployee(empid);
    }

	
	public void deleteEmployee(int employeeId) {
		dao.deleteEmployee(employeeId);
		
	}
}
