package com.employeemanagement.service;

import java.util.List;

import com.employeemanagement.model.Employee;

public interface EmployeeService {

	
	
	void saveEmployee(Employee employee);
	
	void updateEmployee(Employee employee);
	
	

	List<Employee> findAllEmployees(); 
	
	
	void deleteEmployee(int employeeId);

	Employee getEmployee(int employeeId);
	
}
