package com.employeemanagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.employeemanagement.model.Employee;
import com.employeemanagement.service.EmployeeService;

@Controller
public class EmployeeController  extends WebMvcConfigurerAdapter{

	int employeeId;
    String registration="registration";
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	MessageSource messageSource;

	@GetMapping("/list")
	public ModelAndView listEmployees() {
		
		List<Employee> employees = employeeService.findAllEmployees();
		
		return new ModelAndView("allemployees","employees",employees);
	}
	
	@RequestMapping(value = { "/" })
	public String newEmployee(ModelMap model) {
		Employee employee = new Employee();
		model.addAttribute("employee", employee);
		model.addAttribute("edit", false);
		return registration;
	}

	@PostMapping(value="/save")
	public String saveEmployee(@Valid @ModelAttribute("employee") Employee employee, BindingResult result,
			ModelMap model) {
	

		if (result.hasErrors()) {
			return registration;
		}
		else
		{
			if(employee.getId()!=0)
			{
				
				
				employeeService.updateEmployee(employee);
			}
			else
			{
			
		employeeService.saveEmployee(employee);
			}
			
		return "redirect:/list";
		}
	}

	@GetMapping(value = "/deleteEmployee")
    public ModelAndView deleteEmployee(HttpServletRequest request) {
       employeeId = Integer.parseInt(request.getParameter("id"));
	    employeeService.deleteEmployee(employeeId);
        return new ModelAndView("redirect:/list");
    }
	    @GetMapping(value = "/editEmployee")
	    public ModelAndView editContact(HttpServletRequest request) {
	 employeeId = Integer.parseInt(request.getParameter("id"));
	    	        Employee employee = employeeService.getEmployee(employeeId);
	        ModelAndView model = new ModelAndView("registration");
	        model.addObject("employee", employee);
	 
	        return model;
	    }

}
