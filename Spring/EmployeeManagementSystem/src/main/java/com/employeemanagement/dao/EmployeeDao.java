package com.employeemanagement.dao;

import java.util.List;

import com.employeemanagement.model.Employee;

public interface EmployeeDao {

	

	void saveEmployee(Employee employee);
	
	
	void updateEmployee(Employee employee);
	List<Employee> findAllEmployees();

	

	void deleteEmployee(Integer employeeId);

	Employee getEmployee(int empid);

}
