package com.employeemanagement.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.employeemanagement.model.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	SessionFactory sessionFactory;
	
	

	public void saveEmployee(Employee employee) {
		
		sessionFactory.getCurrentSession().save(employee);
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> findAllEmployees() {
	return sessionFactory.getCurrentSession().createQuery("from Employee").list();
	
	
	}


	public void deleteEmployee(Integer employeeId) {
        Employee employee = (Employee) sessionFactory.getCurrentSession().load(
                Employee.class, employeeId);
        if (null != employee) {
            this.sessionFactory.getCurrentSession().delete(employee);
        }
 
    }
	 
    public Employee getEmployee(int empid) {
   
    return (Employee)sessionFactory.getCurrentSession().get(Employee.class, empid);
   
    	
    	
		
    }
	public void updateEmployee(Employee employee) {
		sessionFactory.getCurrentSession().update(employee);
		
	}

	
 }
