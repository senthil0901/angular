import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { SearchCustomersComponent } from './search-customers/search-customers.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { LoginComponent } from './login/login.component';
import { AuthguardService } from './authguard.service';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  { path: 'add', component: CreateCustomerComponent },
  { path: 'customer', component: CustomersListComponent,canActivate:[AuthguardService]},
  { path: 'edit', component: EditCustomerComponent },
  { path: 'findbyage', component: SearchCustomersComponent },
  { path: '', redirectTo: 'customer', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent,canActivate:[AuthguardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
