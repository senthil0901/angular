import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
 
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
 
  private baseUrl = 'http://localhost:8080/api/customers';
 
  constructor(private http: HttpClient) { }

  createCustomer(customer: any): Observable<any> {
    return this.http.post(this.baseUrl, customer);
  }

  getCustomersList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

 
  getCustomer(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
 
  
 
  /*updateCustomer(id: number, value: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }*/
 
  deleteCustomer(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
 
  
 
  getCustomersByAge(age: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/age/${age}`);
  }
 
  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl);
  }

 /* getCustomerId(id:number){
    return this.http.get(`${this.baseUrl}`+"/"+id);
 }*/

 updateCustomer(customer: Object): Observable<Object> {
  return this.http.put(`${this.baseUrl}` + `/update`, customer);
 }

 
}