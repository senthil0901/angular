import { Component, OnInit } from '@angular/core';
import { ComponentDeactivate } from '../component-deactivate';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit, ComponentDeactivate {

  isDirty : false;
  constructor() { }
 
  canDeactivate(): boolean {
    return !this.isDirty;
  }

  ngOnInit(): void {
  }

}
