import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentDeactivate } from './component-deactivate';

@Injectable({
  providedIn: 'root'
})
export class DirtyCheckGuard implements CanDeactivate<ComponentDeactivate> {
  canDeactivate(component: ComponentDeactivate, currentRoute: ActivatedRouteSnapshot, 
    currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
   if(component.canDeactivate()){
     return true;
   }else{
     return confirm("You have unsaved changes, do you want to go next page?");
   }
   return component.canDeactivate();
  }
  
  
}
   