import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { DirtyCheckGuard } from './dirty-check.guard';

const routes: Routes = [
  {path: 'first', component: FirstComponent, canDeactivate:[DirtyCheckGuard]},
  {path: 'second', component: SecondComponent},
  {path: '**', redirectTo: 'first'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
