import { Observable } from 'rxjs';

export interface ComponentDeactivate {
   canDeactivate: () => boolean | Observable<boolean>
}
