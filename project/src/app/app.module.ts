import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { SampleComponent } from './sample/sample.component';
import { EmployeetitlePipe } from './employeetitle.pipe';
import { HomeComponent } from './home/home.component';
import { HomepipePipe } from './homepipe.pipe';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeecountComponent } from './employeecount/employeecount.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    SampleComponent,
    EmployeetitlePipe,
    HomeComponent,
    HomepipePipe,
    EmployeelistComponent,
    EmployeecountComponent,
    LifecycleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
        