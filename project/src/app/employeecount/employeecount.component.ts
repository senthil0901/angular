import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employeecount',
  templateUrl: './employeecount.component.html',
  styleUrls: ['./employeecount.component.css']
})
export class EmployeecountComponent implements OnInit {

 /* all:number=5;
  male:number=2;
  female:number=3;*/

  @Input()
  all: number;
  @Input()
  male:number;
  @Input()
  female:number;

  constructor() { }

  ngOnInit(): void {
  }

  selectedRadioButtonValue: string="Male";

  @Output()
  countRadioButtonSelectionChanged: EventEmitter<string> = new EventEmitter<string>();

  onRadioButtonSelectionChange(){
    this.countRadioButtonSelectionChanged.emit(this.selectedRadioButtonValue);
  }

}
    