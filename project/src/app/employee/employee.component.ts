import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  emp:Employee={
    id:100,name:"Ram",date:"10/10/2000",gender:"female",salary:20000,skill:[
      {skill_id:10,skill_name:"Java"},
      {skill_id:11,skill_name:"C++"},
      {skill_id:12,skill_name:"J2EE"},
    ]
  }

  showDetails:boolean=false;

  toggleDetails(): void{
    this.showDetails=!this.showDetails;
  }

  jsonVal={name:"Raj",age:25,location:"chennai"};

  months=["jan","feb","mar","apr","jun","jul","aug","sep","oct","nov","dec"];

}
