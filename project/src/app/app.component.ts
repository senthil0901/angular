import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
 /* template: `
       <h2 class="one">Hello world</h2>
  `,
  styles: [`.one{
    color:red;
  }`]*/
  templateUrl:'./app.component.html',
  styleUrls:['./app.component.css']
})
export class AppComponent {
  title = 'project';

  pageHeader: string ="Employee Details";

  imagePath:string="download.jfif";

  isDisabled: boolean=false;

  click(){
    console.log("Button clicked");
  }
}
